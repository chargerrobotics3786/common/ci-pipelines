# CI Pipelines

This repository maintains all common CI Pipelines for Charger Robotics.

Below are all of the available pipelines and sample usages:

## Java Library Pipeline
```yaml
include:
    - project: 'chargerrobotics3786/common/ci-pipelines'
      ref: v0.1.1
      file: '/pipelines/java/java-library.yml'
```